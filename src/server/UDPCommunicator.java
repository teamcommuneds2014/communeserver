package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class UDPCommunicator {
	
	private static final int SOCKET_TIMEOUT = 10000;

	private DatagramSocket socket;
	
	private int bufferSize;
	
	public UDPCommunicator(int port,int buffSize) {
		this.bufferSize = buffSize;
		initSocket(port);
		
	}
	
	private void initSocket(int port) {
		try {
			this.socket = new DatagramSocket(port);
			socket.setSoTimeout(SOCKET_TIMEOUT);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public DatagramPacket listenForMessage() throws IOException {
		byte[] buf = new byte[bufferSize];
		
		DatagramPacket receive = new DatagramPacket(buf, bufferSize);
		
		socket.receive(receive);

		return receive;
	}
	
	public synchronized boolean sendMessage(String msg, InetAddress destIP, int destPort) {
		DatagramPacket sendPacket;
		
		try {
			sendPacket = new DatagramPacket(msg.getBytes(), msg.getBytes().length, destIP, destPort);
			socket.send(sendPacket);
		} catch (IOException e) {
			return false;
		}
		
		return true;
	}
	
	public void closeConnection() {
		socket.close();
	}
	
}
