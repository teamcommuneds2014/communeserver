package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPException;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

/**
 * Rendez-Vous Server for the Commune Project
 * @author Lukas Burkhalter
 *
 */
public class MeetServer extends Thread {
	
	//Server constants
	public static final int SERVER_PORT =8081;
	public static final int SERVER_PACKET_SIZE = 65535;
	private static final int NUM_SLAVE_THREADS = 2;
	
	// GCM keys
	private static final long senderId =  469440637169L; // your GCM sender id
	private static final String password = "AIzaSyBb3Org4qG-BA24vvmP69_aG7LEbW0ZdEk";

	//Communication classes
	private SmackCcsClient gcmService; 
	private UDPCommunicator communicator;
	
	private static final Logger logger = Logger.getLogger(MeetServer.class.getName());
	
	private List<Responder> responders;
	
	/**
	 * The thread pool for handling the responses
	 */
	private ExecutorService responderPool;
	
	/**
	 * The thread pool for handling the Distributors
	 */
	private ExecutorService handlerPool;
	
	/**
	 * Indicates if the server is running
	 */
	private boolean isRunning = false;
	
	public MeetServer() {
		responders = Collections.synchronizedList(new ArrayList<Responder>()); 
	}
	
	/**
	 * Initialize the Communication classes
	 */
	private void initialize() {
		if(this.communicator==null)
			this.communicator = new UDPCommunicator(SERVER_PORT,SERVER_PACKET_SIZE);
		gcmService = new SmackCcsClient();
	    try {
	    	gcmService.connect(senderId, password);
		} catch (XMPPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SmackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Start the server
	 */
	public synchronized void startThread() {
        isRunning = true;
        initialize();
        super.start();
	}
	
	/**
	 * Stop the server
	 */
	public synchronized void stopThread(){
        isRunning = false;
        communicator.closeConnection();
        gcmService.disconnect();
	}

	
	
	@Override
	public void run() {
		super.run();
		runServer();
	}
	
	/**
	 * Runs the Server
	 */
	private void runServer() {
		responderPool = Executors.newFixedThreadPool(NUM_SLAVE_THREADS);
		handlerPool = Executors.newFixedThreadPool(NUM_SLAVE_THREADS);
		
		while(isRunning) {
			try {
				if(!gcmService.isGCMConnected())
					initialize();
				DatagramPacket incomingMsg = communicator.listenForMessage();
				if(incomingMsg!=null) {
					logger.log(Level.INFO, "Received UDP message from: "+incomingMsg.getAddress().toString());
					
					byte[] payload = incomingMsg.getData();
					String msg= new String(payload,0,incomingMsg.getLength());
					
					logger.log(Level.INFO, "Msg String:"+msg);
					
					try {
						@SuppressWarnings("unchecked")
						Map<String, Object>  jsonObject = (Map<String, Object>) JSONValue.parseWithException(msg);
						
						String type = (String) jsonObject.get(MessagesUtils.TYPE_KEY);
						
						if(type != null)
							if(type.equals(MessagesUtils.TYPE_ASK_IP)) {
								logger.log(Level.INFO, "Give it to Responder");
								responderPool.execute(new Responder(incomingMsg,gcmService,communicator));
							}
							else  {
								logger.log(Level.INFO, "Give it to Distributor");
								handlerPool.execute(new Distributor(jsonObject,incomingMsg.getAddress(),incomingMsg.getPort()));
							}
						
					} catch (ParseException pe) {
						
					}
				}
			} catch (SocketTimeoutException e) {
				//timeout retry
			} catch (IOException e) {
			
			}
		}
		
		responderPool.shutdown();
		handlerPool.shutdown();
		
		try {
			responderPool.awaitTermination(5000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			logger.log(Level.INFO, "ResponderPool has Problems with termination");
		}
		try {
			handlerPool.awaitTermination(5000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			logger.log(Level.INFO, "HandlerPool has Problems with termination");
		}
	    responderPool = null;
	    handlerPool = null;
	}
	
		
	private class Distributor implements Runnable {
		
		Map<String, Object>  msgToHandle;
		
		private InetAddress fromIP;
		private int fromPort;
		
		public Distributor(Map<String, Object>  jsonObject,InetAddress fromIP,int fromPort) {
			this.msgToHandle = jsonObject;
			this.fromIP = fromIP;
			this.fromPort = fromPort;
		}
		
		private void hanlde(Responder resp) {
			String msgID = (String) msgToHandle.get(MessagesUtils.MSG_ID_KEY);
			if(msgID==null)
				return;
			
			resp.handleClientIPMsg(msgID,fromIP,fromPort);
			
		}
		
		@Override
		public void run() {
			String type = (String) msgToHandle.get(MessagesUtils.TYPE_KEY);
			
			if(type.equals(MessagesUtils.TYPE_CLIENT_ADDR)) {
				synchronized(responders) {
				      Iterator<Responder> i = responders.iterator();
				      while (i.hasNext())
				          hanlde(i.next());
				}
			}
			else if(type.equals(MessagesUtils.TYPE_FORWRDING)) {
				String ipString = (String) msgToHandle.get(MessagesUtils.CLIENT_IP_KEY);
				long portString = (long) msgToHandle.get(MessagesUtils.CLIENT_PORT_KEY);
				String message = (String) msgToHandle.get(MessagesUtils.MSG_FORWARD_KEY);
				
				InetAddress ia;
				int port;
				try {
					ia = InetAddress.getByName(ipString);
					port = (int) portString;
				} catch (UnknownHostException e) {
					return;
				}
				communicator.sendMessage(message, ia, port);
			}
			
		}
		
	}
	
	/**
	 * Class that handles the incoming messages and does the work for the respond
	 * @author Lukas Burkhalter
	 *
	 */
	private class Responder implements Runnable {
		
		private static final int UDP_RESPOND_TIMEOUT = 5000;
		
		private DatagramPacket dataToHandle;
		
		private SmackCcsClient gcmService; 
		private UDPCommunicator communicator;
		
		// ASKERS address
		private InetAddress fromIP;
		private int fromPort;
		
		//Data from the UDPRequest
		private String toID;
		private String queryID;
		
		private CountDownLatch timeoutLatch = new CountDownLatch(1);
		private Object mutex = new Object();
		private volatile boolean timeout = false;
		
		private final Logger logger = Logger.getLogger(Responder.class.getName());
		
		public Responder(DatagramPacket packet,SmackCcsClient gcmService, UDPCommunicator communicator) {
			this.dataToHandle = packet;
			this.gcmService = gcmService;
			this.communicator = communicator;
		}
		

		/**
		 * Parses the Data, that this Responder should handle and sets the corresponding global fields
		 * @return returns if the Data is a valid
		 */
		@SuppressWarnings("unchecked")
		protected boolean parseDatagram() {
			String msg="";
			Map<String, Object>  jsonObject = null;
			byte[] payload = dataToHandle.getData();
			
			fromIP = dataToHandle.getAddress();
			fromPort = dataToHandle.getPort();
			
			msg= new String(payload,0,dataToHandle.getLength());
			
			
			try {
				jsonObject = (Map<String, Object>) JSONValue.parseWithException(msg);
			} catch (ParseException e) {
				return false;
			}
			
			String type = (String) jsonObject.get(MessagesUtils.TYPE_KEY);
			queryID = (String) jsonObject.get(MessagesUtils.MSG_ID_KEY);
			toID = (String) jsonObject.get(MessagesUtils.NAME_KEY);
			
			boolean isNull = type==null || toID ==null;
			
			if(isNull || !type.equals(MessagesUtils.TYPE_ASK_IP))
				return false;

			return true;
		}
		
		/**
		 * Sends the GCM request to the GCM server
		 * @return returns a boolean if the sending was successfully done
		 */
		private boolean sendGCMMessage() {
	        String messageId = gcmService.nextMessageId();
	        Map<String, String> payload = MessagesUtils.getGCMAskMessagePayload(fromIP.toString(),fromPort,queryID);
	        String collapseKey = "sample";
	        Long timeToLive = 10L;
	        String message = SmackCcsClient.createJsonMessage(toID, messageId, payload, null, timeToLive, false);
	        
	        try {
				gcmService.send(message);
			} catch (NotConnectedException e) {
				return false;
			}
	        
	        return true;
		}
		
		@Override
		public void run() {
			processData();
		}
		
		/**
		 * Processing the data and generate the respond
		 */
		private void processData() {
			responders.add(this);
			logger.log(Level.INFO, "Process UDP message..");
			
			if(!parseDatagram()) {
				logger.log(Level.INFO, "Received invalid UDP message");
				return;
			}
			
			logger.log(Level.INFO, "Sending Message to GCM Server");
			
			if(!sendGCMMessage()) {
				logger.log(Level.INFO, "Sending message to GCM failed");
				return;
			}
	
			logger.log(Level.INFO, "Waiting for UDP respond..");
			try {
				timeoutLatch.await(UDP_RESPOND_TIMEOUT, TimeUnit.MILLISECONDS);
				synchronized(mutex) {
					if(timeoutLatch.getCount()>0) {
						logger.log(Level.INFO, "UDP respond Timeout");
						timeout = true;
					}
					else {
						logger.log(Level.INFO, "DONE");
					}
					responders.remove(this);
				}
			} catch (InterruptedException e) {
				
			}
			
		}
		
		public void handleClientIPMsg(String msgID, InetAddress handleIP,int handlePort) {
			synchronized(mutex) {
				if(queryID!=null && msgID.equals(queryID) && !timeout) {
					logger.log(Level.INFO, "UDP respond received");
					
					String msg = MessagesUtils.getUDPClientAddressMessage(handleIP, String.valueOf(handlePort), msgID);
					
					if(fromIP == null || fromPort == 0)
						return;
					
					if(this.communicator.sendMessage(msg,fromIP,  fromPort)) {
						logger.log(Level.INFO, "UDP message send to Asker");
					}
					else {
						logger.log(Level.INFO, "UDP message send to Asker failed");
					}
					
					timeoutLatch.countDown();	
				}
			}
		}
		
		
	}

	
	
	
}
