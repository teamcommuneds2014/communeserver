package server;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

/*
JSON Messages:

A -> request IP -> B

A ->UDP server
{
	"type": "ASK_IP",
	"nameGCM" : "GCM id of B",
	"clientPort" : "port on which A wants to talk with b",
	"queryID": "unique ID for the whole query"
}

serverGCM -> B
payload:
{
	"type": "ASK_IP",
	"clientPort" : "port on which A wants to talk with b",
	"queryID": "unique ID for the whole query"
}

B -> serverGCM
payload 
{
	"type": "CLIENT_IP",
	"clientIP": "Ip of B",
	"clientPort" : "port of b",
	"queryID": "unique ID for the whole query"
}

server UDP-> A
{
	"type": "CLIENT_IP",
	"clientIP": "Ip of B",
	"clientPort" : "port of b",
	"queryID": "unique ID for the whole query"
}
*/

/**
 * Defines Constants and Functions for the predefined messages of the server
 * @author lukas
 *
 */
public class MessagesUtils {
	
	public static final String TYPE_KEY = "type";
	
	public static final String TYPE_ASK_IP = "ASK_IP";
	
	public static final String TYPE_CLIENT_ADDR = "CLIENT_IP";
	
	public static final String TYPE_FORWRDING = "FORWARD";
	
	public static final String MSG_ID_KEY = "queryID";
	
	public static final String NAME_KEY = "nameGCM";
	
	public static final String CLIENT_IP_KEY = "clientIP";
	
	public static final String CLIENT_PORT_KEY = "clientPort";
	
	public static final String MSG_FORWARD_KEY= "message";
	
	public static  Map<String, String> getGCMAskMessagePayload(String fromIpIN, int fromPort,String queryID) {
	        Map<String, String> payload = new HashMap<String, String>();
	        String fromIp = fromIpIN.toString().substring(1);
	        payload.put(TYPE_KEY, TYPE_ASK_IP);
	        payload.put(CLIENT_IP_KEY, fromIp);
	        String fromPortS = String.valueOf(fromPort);
	        payload.put(CLIENT_PORT_KEY, fromPortS);
	        payload.put(MSG_ID_KEY, queryID);
	        return payload;
	}
	
	@SuppressWarnings("unchecked")
	public static String getUDPClientAddressMessage(InetAddress clientIpIn, String clientPort, String queryID) {
		JSONObject jsonMsg = new JSONObject();
		String clientIp = clientIpIn.toString().substring(1);
		jsonMsg.put(TYPE_KEY, TYPE_CLIENT_ADDR);
		jsonMsg.put(CLIENT_IP_KEY, clientIp);
		jsonMsg.put(CLIENT_PORT_KEY, clientPort);
		jsonMsg.put(MSG_ID_KEY, queryID);
		return jsonMsg.toJSONString();
		
	}
	
}
