# JSON Messages: # 

**A UDP-> request IP -> B** 

**A UDP-> server**
{
	"type": "ASK_IP",
	"nameGCM" : "GCM id of B",
	"queryID": "unique ID for the whole query"
}

**server GCM-> B**
payload:
{
	"type": "ASK_IP",
	"clientIP": "Ip of A“,
	"clientPort" : "port on which A wants to talk with b",
	"queryID": "unique ID for the whole query"
}

**B UDP-> Server**
{
	"type": "CLIENT_IP",
	"queryID": "unique ID for the whole query"
}

**Server UDP-> A**
{
	"type": "CLIENT_IP",
	"clientIP": "Ip of B“,
	"clientPort" : "port on which B wants to talk with a“,
	"queryID": "unique ID for the whole query"
}

// peer2peer
**A UDP-> B**
{
	"type": "CONNECT",
	"queryID": "unique ID for the whole query"
}

**B UDP-> A**
{
	"type": "CONNECT_OK",
	"queryID": "unique ID for the whole query"
}

//Forwarding msg, if peer2peer fails
**A UDP-> Server -> UDP B**
{
	"type": „FORWARD“,
	"clientIP": "Ip of B“,
	"clientPort" : "port of B“,
	"message“: "message for B in JSON"
}


